"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
   #Deciding what speed to use based on brevet_dist_km
    print(brevet_start_time)
    if control_dist_km > brevet_dist_km:
       control_dist_km = brevet_dist_km
   #Calculating total time given control_dist
    myli = [34,32,30,28,26] #list of speeds
    tempkm = control_dist_km #changing distance
    totaltime = 0 #time counter
    check = 1 #check to exit while
    ind = 0 #ind to track current speed
    while (check == 1): #Incrementally reduces tempkm by 200 to calculate total time
        if (tempkm - 200) >= 0:
            totaltime += (200/myli[ind]) #Time calculation
            if ind < 3:
               ind += 1
            tempkm -= 200
            
        else:
            totaltime += (tempkm/myli[ind]) #Time calculation LAST edit
            check = 0 #Finish checking
   
      
    #Translating the time into hour only and minute only for easy transformation
    hronly = math.floor(totaltime)
    minonly = round((totaltime- hronly) * 60)
    rslttime = hronly, minonly
    #print("This is my result time: ", rslttime)

    #Shift time by rslttime NEED TO SHIFT
    aftertime = arrow.get(brevet_start_time)
    aftertime = aftertime.shift(hours =+ rslttime[0], minutes =+ rslttime[1])

    return aftertime.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    #Calculating total time given control_dist
    cmyli = [15,15,15,11.428,13.333] #All speeds
    tempkm = control_dist_km #Changing distance
    totaltime = 0 #Time counter
    check = 1 #Check to exit loop
    ind = 0 #index to track current speed
    if tempkm > 600:
       totaltime += 600/15
       tempkm -= 600
       ind = 3
    while (check == 1): #Incrementally reduces tempkm by 200 to calculate total time
        if ind >= 3:
           if (tempkm - 450) >= 0:
              totaltime += (450/cmyli[ind]) #Time calculations
              if ind < 3:
                 ind += 1
              tempkm -= 450
           else:
              totaltime += (tempkm/cmyli[ind]) #Time calculations
              check = 0 #finish loop
        else:
           if (tempkm - 200) >= 0:
              totaltime += (200/cmyli[ind]) #Time calculations
              if ind < 3:
                 ind += 1
              tempkm -= 200          
           else:
              totaltime += (tempkm/cmyli[ind]) #Time calculations
              check = 0 #finish loop

    #Translating the time into hour only and minute only for easy transformation
    hronly = math.floor(totaltime)
    minonly = round((totaltime - hronly) * 60)
    rslttime = hronly, minonly

    #print(control_dist_km)
    #Rider rules set times 
    if control_dist_km == 0:
       rslttime = (1,00)
    elif control_dist_km == 10:
       rslttime = (1,30)
    elif control_dist_km == 15:
       rslttime = (1,45)
    elif control_dist_km == 20:
       rslttime = (2,00)
    elif control_dist_km == 25:
       rslttime = (2,15)
    elif control_dist_km == 30:
       rslttime = (2,30)
    elif control_dist_km == 35:
       rslttime = (2,45)
    elif control_dist_km == 40:
       rslttime = (3,00)
    elif control_dist_km == 45:
       rslttime = (3,15)
    elif control_dist_km == 50:
       rslttime = (3,30)
    elif (brevet_dist_km == 200) & (control_dist_km == 200):
       rslttime = (13,30)
    elif (brevet_dist_km == 300) & (control_dist_km == 300):
       rslttime = (20,00)
    elif (brevet_dist_km == 400) & (control_dist_km == 400):
       rslttime = (27,00)
    elif (brevet_dist_km == 600) & (control_dist_km == 600):
       rslttime = (40,00)
    elif (brevet_dist_km == 1000) & (control_dist_km == 1000):
       rslttime = (75,00)

    #Shift time by rslttime!
    aftertime = arrow.get(brevet_start_time)
    aftertime = aftertime.shift(hours =+ rslttime[0], minutes =+ rslttime[1])

    return aftertime.isoformat()
